angular.module('app')
    .directive('a', [function() {
        return {
            restrict: 'E',
            link: function(scope, elem, attrs) {
                if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                    elem.on('click', function(event) {
                        event.preventDefault();
                        event.stopPropagation();



                    });
                }
            }
        };
    }])
    .directive('repeatDone', ['$timeout', function($timeout) {
        return function(scope, element, attrs) {
            if (scope.$last) {

                var _parent = element.parent()
                angular.forEach(_parent.find('li'), function(value, index) {

                    ! function outer(index) {
                        //Execute some code once the ngRepeat has completed
                    }(index);

                });

            }
        };
    }])
    .directive(
        'setHeight', ['$window', function($window) {
            var directive = {
                link: function(scope, element, attr) {

                    var offset = Number(attr.setHeight) * -1,
                        wh = $window.innerHeight;

                    var resize = function() {
                        offset = Number(attr.setHeight) * -1,
                            wh = $window.innerHeight;

                        element.css({
                            height: (wh + offset) + 'px'
                        });
                    }


                    resize()

                    $($window).on("resize", resize)

                }
            };
            return directive;
        }])
