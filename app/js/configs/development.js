/**
 * Created by redroger on 2/18/15.
 */
angular.module('app').factory(
    'APP_OVERRIDE', ['$rootScope',
        function($rootScope) {
            var override = {
                //loggerHost:"localhost" //used to override the debug functionality
                env: 'development',
            };



            return override;
        }
    ]
)
