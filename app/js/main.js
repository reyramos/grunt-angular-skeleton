// Require JS  Config File
require
    ({
            baseUrl: '/js/',
            name: "main",
            removeCombined: true,
            paths: {
                'angular': '../../lib/angular/angular',
                'angular-resource': '../../lib/angular-resource/index',
                'angular-route': '../../lib/angular-route/index',
                'angular-cookies': '../../lib/angular-cookies/index',
                'angular-sanitize': '../../lib/angular-sanitize/index',
                'angular-animate': '../../lib/angular-animate/index',
                'angular-touch': '../../lib/angular-touch/index',



                /* CONFIGS */
                'development': 'configs/development'
            },
            shim: {
                'app': {
                    'deps': [
                        'angular',
                        'angular-route',
                        'angular-resource',
                        'angular-sanitize',
                        'angular-animate',
                        'angular-cookies',
                        'angular-touch'
                    ]
                },
                'angular-route': {
                    'deps': ['angular']
                },
                'angular-resource': {
                    'deps': ['angular']
                },
                'angular-cookies': {
                    'deps': ['angular']
                },
                'angular-sanitize': {
                    'deps': ['angular']
                },
                'angular-animate': {
                    'deps': ['angular']
                },
                'angular-touch': {
                    'deps': ['angular']
                },


                // THIS IS ONLY DURING DEVELOPMENT ENVIROMENT CHANGES
                'development': {
                    'deps': ['app']
                },

                'routes': {
                    'deps': ['app']
                },

                'controllers/application': {
                    'deps': [
                        'app'
                    ]
                },
                'directives/global': {
                    'deps': [
                        'app'
                    ]
                }
            }
        }, [
            'require',
            'development',
            'routes',
            'directives/global',
            'controllers/application'
        ],
        function(require) {
            return require(['bootstrap'])
        }
    );
