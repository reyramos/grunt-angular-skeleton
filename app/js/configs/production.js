/**
 * Created by redroger on 3/14/15.
 */
angular.module('app').factory(
    'APP_OVERRIDE', ['$rootScope',
        function($rootScope) {

            $rootScope.modalContext = {}

            var override = {
                //loggerHost:"localhost" //used to override the debug functionality
                env: 'production'

            };
            return override;
        }
    ]
)
